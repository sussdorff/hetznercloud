URL: https://www.tmjohnson.co.uk/posts/k8s/

# Setup HetznerCloud for Gitlab CI/CD Pipeline

We have been a Hetzner Customer for centuries (well, decades, but as this passes a century boundary.... oh nevermind) and therefore were keen on deploying our cluster on HetznerCloud. 

I dabbled with it back in December 2019 (pre-Corona) when learning about Docker, Docker Swarm and the basics of Kubernetes. Sadly, not much came out of if (except two machines running idle all the time).

Coming back, taking Udemy Courses on Gitlab CI/CD I realized they do depend on a Kubernetes cluster(ish). So why not engage again.

This is a step by step guide.

## Install Hetzner cube

You need to install Kubernetes and Hetzner-cube in order to get this to work. The following worked in August 2020 on a mac:


## Setup HetznerCloud

First of all, I assume you already have a hetzner cloud account. If not, get one :-).

Next generate a project (apart from the default one). I called mine "gitlab". Make sure you are actually in the project by looking at the top left where there is a dropdown menu. This should be set to "gitlab" (or whatever you call your project).

Then head over to the Security section (click the key symbol in the right sidebar menu) and add a new API Token (I called mine... gitlab :-)). Make sure to copy and **safe** the token into a secure location (e.g. the excellent 1Password) as you will need for setting up the hetzner-cloud context.

`hetzner-kube context add gitlab` (I call it gitlab for consistency, you can differ)

You will immediately be asked for your API token.

## Create your SSH key
You would assume that is easy. But the default RSA 3072 combination was not liked. Neither was ed25519 keys. Turns out, I needed to use PEM in order to avoid
"`parse key failed:ssh: cannot decode encrypted private keys`"

So, how should you then generate your key? Here is what worked for me:

`ssh-keygen -f hetzner -t rsa -b 4096 -m PEM`

Note that I name my SSH Key. Most tutorials don't bother, but if you are working on Gitlab CI/CD then you are most likely having more than one SSH Key. 

After creation you can now safely add the SSH Key to hetzner cube

`hetzner-kube ssh-key add -n HetznerSSH --private-key-path ~/.ssh/hetzner --public-key-path ~/.ssh/hetzner.pub`


## Setup Kubernetes in your cluster

Get yourself a floating IP Address and configure it in cloud-init-config - I use one in Fürstenau.

First create your cluster with two worker nodes in Fürstenau Datacenter

    hetzner-kube cluster create --name gitlab -k HetznerSSH -w 2 \
      --master-server-type cx21 --cloud-init cloud-init-config \
      --datacenters fsn1-dc14

Deploy some addons

    hetzner-kube cluster addon install -n gitlab helm
    hetzner-kube cluster addon install -n gitlab hetzner-csi

    hetzner-kube cluster addon install -n gitlab nginx-ingress-controller
    

## Configure floating ip

Get the code for floating ip management

    git clone https://github.com/cbeneke/hcloud-fip-controller.git
    kubectl create namespace fip-controller
    kubectl create -f hcloud-fip-controller/deploy/rbac.yaml
    kubectl create -f hcloud-fip-controller/deploy/daemonset.yaml

Edit your fip-controller.yaml to contain the token and the floating ip. After that apply

    kubectl apply -f fip-controller


    
## Install CERT Manager
    kubectl create namespace cert-manager
    kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.14/deploy/manifests/00-crds.yaml
    helm repo add jetstack https://charts.jetstack.io
    helm install cert-manager --namespace cert-manager jetstack/cert-manager
    


Then save your kubernetes config in `~/.kube/config` so `kubectl` can start working.

    hetzner-kube cluster kubeconfig gitlab -f

# Add storage (obsolete?)

First add your Hetzner Token to csi-secret.yml and then apply

`kubectl apply -f csi-secret.yml`

Now deploy the CSI Driver

`kubectl apply -f https://raw.githubusercontent.com/kubernetes/csi-api/release-1.14/pkg/crd/manifests/csidriver.yaml`

`kubectl apply -f https://raw.githubusercontent.com/kubernetes/csi-api/release-1.14/pkg/crd/manifests/csinodeinfo.yaml`

Now you are ready to install the Devices for Kubernetes on Gitlab
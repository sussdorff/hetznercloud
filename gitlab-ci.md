# Setup development pipeline

In order for us to develop code, each of the packages of ]po[ is in it's own repository. This allows us to keep the repositories separate though we do have to merge the together in the deployment process.

Therefore a commit (ideally with tags and/or in master) would trigger a new build of the docker container. 

Sadly we have more than one docker container. Depending on the kind of package it resides in one or the other docker container. And as we don't know for sure which one the developer is using, we need to build all of them?

# Actual building

I order to build code in gitlab ci we are using kaniko based on documentation found on the docker pages:

    build:
      stage: build
      image:
        name: gcr.io/kaniko-project/executor:debug
        entrypoint: [""]
      script:
        - mkdir -p /kaniko/.docker
        - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
        - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA --destination $CI_REGISTRY_IMAGE:latest
$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
      only:
        - tags

Note the we are only building this when we have a new tag for the docker registry. This allows us to manually add a tag to the Docker Main registry. We also automatically add the latest tag to it.

In case we want to split this up so the image is always created, but only tagged when we actually have a tag, we can split the phase into a build phase and a push_when_tagged like this:

    stages:
      - build
      - push

    build:
      stage: build
      image:
        name: gcr.io/kaniko-project/executor:debug
        entrypoint: [""]
      script:
        - mkdir -p /kaniko/.docker
        - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
        - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
      only:
        refs:
          - master

    PushWithTags:
      stage: push
      services:
        - docker:dind
      only:
        - tags
      before_script:
        - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
        - docker version
        - docker info
      after_script:
        - docker logout registry.gitlab.com
      script: 
        - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
        - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
        - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG

Note that we had to run docker in docker for the tag pushing. Probably there is an easier way using kaniko, but this is the way I learned it in my studies so far. Probably we can have build commands where one does contain a tag and the other one doesn't ...

## Build multiple containers based on committed branch

Lets assume the following scenario. We have a commit in cognovis-core which needs to propagate through the docker containers.

1. **erp4projects** - This is where we keep the docker image for a pristine ]project-open[ installation with out custom code
2. **erp4translation** - This is the base container for customers
3. **customer** - For each customer we actually have our own image as well

**IDEA** - Keep the erp4translation and customer as branches of the main erp4projects. This way we can keep all the images in one place.

**IDEA** - We need to create and push the container for the database as well, so maybe an **erp4project-db** is in order with postgres. Then the build could run all the scripts together. Need to figure out how we can actually test this though...

This means a commit in cognovis-core needs to trigger a rebuild in **erp4projects** which then triggers a rebuild of **erp4translation** which then triggers the customers.

In order to handle the commit to cognovis-core to trigger the correct rebuild we need to run multi project deployment as desribed at [Gitlab](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html).

Also take a look at https://medium.com/faun/gitlab-pipeline-to-run-cross-multiple-projects-3563af5d6dca 

## Handling authorization

    docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY

Sadly this works only on project level.

as a workaround i currently created a deploy token in group level named gitlab-deploy-token and set the variables CI_DEPLOY_USER and CI_DEPLOY_PASSWORD myself at group level. (manually)




# Build your CI Pipeline

## Setup a local gitlab runner to test your .gitlab-ci.yml
When creating your .gitlab-ci.yml you might run into issues with the actual execution of the pipeline. To test it locally it therefore makes sense to test it with a gitlab runner on your machine

### Installing a Gitlab Runner on MacOS
    $ sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64
    $ sudo chmod +x /usr/local/bin/gitlab-runner

make sure it runs correctly
    $ gitlab-runner

### Executing the pipeline

Go into the directory of your gitlab project which already needs to contain a .gitlab-ci.yml.
Now you can execute the jobs defined in .gitlab-ci.yml. Keep in mind the jobs are the name given in the yaml and are case sensitive.

For more details check out [Debugging Gitlab CI pipelines locally](http://campfirecode.io/debugging-gitlab-ci-pipelines/).

### Debugging

Make good use of the echo command for the debugging.

    pages:
      script:
        - .....
        - echo $CI_JOB_STAGE

### SSH Keys 

In order for you to use the SSH Keys properly, first generate a key WITHOUT passphrase.

Store the PRIVATE Key in variable. For testing you can still use it as a variable directly in the .gitlab-ci.yml like this.

    variables:
      STAGING_PRIVATE_KEY: "-----BEGIN OPENSSH PRIVATE ....----END OPENSSH PRIVATE KEY-----"
  
NOTE: You need to replace the linebreaks in the private key file with "\n" otherwise you will get format errors.

In order to use a special config (e.g. if you need to run jumphosts etc.) you can store the config as well like this:

    SSH_CONFIG: "Host *\n\tStrictHostKeyChecking no\n\nHost test-staging\n
        Hostname test.cognovis.de\n
        User projop\n
        Port 7669\n
     
    Host test-prod\n
        Hostname 10.2.0.101\n
        User projop\n
        ProxyJump test-staging\n
        ForwardAgent yes"

Again the "\n" is important. Also the Strict HostKeyChecking, otherwise you will get host key problems in your docker container.

Now, if you have the two variables, you can run them like this in the before_script section to add the ssh key and add the config file.

    before_script:
       - eval $(ssh-agent -s)
       - ssh-add <(echo "$STAGING_PRIVATE_KEY")
       - echo "$SSH_CONFIG" > ~/.ssh/config
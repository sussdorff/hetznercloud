# Setting up Kubernetes with Gitlab
kubectl create namespace monitoring

## API URL

In order to get the API url, find this out from the cluster info.

`kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'`

## CA Certificate

This is a two step process. First you need to get the name of the secrets certificate (usually something like `default-token-XXX`).

`kubectl get secrets` (get the name)

then run the following with the name

`kubectl get secret default-token-[xxxxx] -o jsonpath="{['data']['ca\.crt']}" | base64 --decode`

The output from above is something you can then paste into the CA Certicate section of Gitlab

## Service Token

The service token needs to be generated in order for Gitlab to run actions on the cluster

    kubectl apply -f gitlab-admin-service-account.yml
    kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')

Read the token and paste it in

## install ingres

`hetzner-kube cluster addon install helm -n gitlab`

`hetzner-kube cluster addon install nginx-ingress-controller -n gitlab`